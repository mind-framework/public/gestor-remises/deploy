CREATE DATABASE  IF NOT EXISTS `mind-remis` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mind-remis`;
-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: 10.161.138.101    Database: mind-remis
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `planillas`
--

DROP TABLE IF EXISTS `planillas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `planillas` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_agencia` int unsigned NOT NULL,
  `ts_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `movil` int unsigned NOT NULL,
  `id_chofer` int unsigned DEFAULT NULL,
  `fecha_apertura` date NOT NULL,
  `hora_apertura` time NOT NULL,
  `turno` enum('Día','Noche','Intermedio') NOT NULL,
  `fecha_cierre` date DEFAULT NULL,
  `hora_cierre` time DEFAULT NULL,
  `estado` enum('Abierta','Cerrada','Anulada','Suspendida') DEFAULT 'Abierta',
  `obs` varchar(1000) DEFAULT NULL,
  `estado_movil` enum('Libre','Ocupado') DEFAULT 'Libre',
  `ts_anterior` timestamp NULL DEFAULT NULL,
  `comision_importe` decimal(10,2) unsigned DEFAULT '0.00',
  `comision_porcentaje` int unsigned DEFAULT '20',
  `total_planilla` decimal(10,2) unsigned DEFAULT '0.00',
  `gastos` decimal(10,2) DEFAULT '0.00',
  `combustible` decimal(10,2) DEFAULT '0.00',
  `gastos_agencia` decimal(10,2) DEFAULT '0.00',
  `combustible_agencia` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Índice 4` (`fecha_apertura`,`movil`,`turno`),
  KEY `Índice 2` (`fecha_apertura`,`hora_apertura`,`hora_cierre`),
  KEY `Índice 3` (`movil`,`fecha_apertura`),
  KEY `fk_planillas_1_idx` (`id_agencia`),
  KEY `fk_planillas_3_idx` (`id_chofer`),
  KEY `fk_planillas_2_idx` (`id_agencia`,`movil`),
  CONSTRAINT `fk_planillas_1` FOREIGN KEY (`id_agencia`) REFERENCES `agencias` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_planillas_2` FOREIGN KEY (`id_agencia`, `movil`) REFERENCES `moviles` (`id_agencia`, `movil`),
  CONSTRAINT `fk_planillas_3` FOREIGN KEY (`id_chofer`) REFERENCES `choferes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planillas`
--

LOCK TABLES `planillas` WRITE;
/*!40000 ALTER TABLE `planillas` DISABLE KEYS */;
/*!40000 ALTER TABLE `planillas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-21 16:02:37
