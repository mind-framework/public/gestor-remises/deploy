# deploy

Conjunto de archivos de configuración para la implementación del Gestor de Viajes


## levantar entorno
1 - Clonar proyecto
`git clone https://gitlab.com/mind-framework/public/gestor-remises/deploy.git` 

2 - crear y configurar archivos de entorno
`cp env-example .env`
`cp env/api-common-example.env env/api-common-example.env`

4 - generar certificados
`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certs/server.key -out  certs/server.crt -batch`

3 - Iniciar entorno
`docker-compose up -d`

Nota:
* la primera vez que se inicia el entorno se realizará la creación de las tablas en la bd. este proceso puede tardar varios minutos
